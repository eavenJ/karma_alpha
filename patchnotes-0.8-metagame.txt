attaques Mawashi Geri, Escalade, Jet-Pierres, Aile d'Acier, Canicule, Pied Bruleur, Poing Météore, Marto-Poing, Marteau de Glace, Stratopercut, Hydro-Queue, Rayon Chargé, Eructation, Câlinerie, Ravage Rampant, Hache de Pierre, Force Mystique, Vagues à Lames, Coupe, Vol, Tranch'Herbe, Vent Glace, Griffe Acier, Eclate Griffe, Tranch'Air, Tomberoche, Tir de Boue, Lame d'Air, Crocs Eclair, Crocs Feu, Crocs Givre, Spatio-Rift, Toile Elek, Tunnelier, Coquilame, Aboiement, Coup Victoire, Flying Press, Orage Adamantin, Cavalerie Lourde, Vapeur Féerique, Métalaser et Triple Plongeon passent à 100%
attaques Ultralaser-like passent à 100%
attaques Surchauffe, Tempête Verte, Draco Météore, Psycho Boost et Canon Floral passent à 100%
attaque Piqué et Laser Météore passe à 100%
attaques Feu Glacé et Eclair Gelé passent à 100%
attaque Poltergeist passe à 100%
attaque Bang Sonique passe à 150/100%
attaques Ultimapoing, Tricherie, Cavalerie Lourde et Canicule passent à 100/100%
attaque Griffe Ombre, Coupe Psycho et Tranche-Nuit passent à 80/100%
attaque Lame d'Air passe à 80/100%
attaque Torrent d'Coups passe à 75/100%
attaque Furie-Bond passe à 70/100%
attaque Tranch'Aqua passe à 60/100%
attaque Poing Boost passe à 50/100%
attaques Lancécrou, Coup Double, Osmerang, Double Baffe et Double Volée passent à 40/100%
attaque Pilonnage passe à 30/100%
attaques Plumo-Queue, Charge-Os, Boule Roc, Rafale Ecailles et Dard-Nuée passent 25/100%
attaques Combo-Griffe et Poing Comète passe à 20/100%
attaques Torgnoles et Furie passent à 100%
attaques Etreinte et Ligotage passent à 100%
attaques Souffle Glacé et Yama Arashi passent à 100%
attaques Draco-Queue et Projection passent à 100%
attaques Croc Fatal, Ire de la Nature et Cataclysme passent à 100%
attaque Sonicboom passe à 100%
attaque Secrétion passe à 100%
attaques Poudre Toxik, Para-Spore, Gaz Toxik, Cage-Eclair et Doux Baiser passent à 100%
attaques Ballon Brûlant et Cradovague passent à 110/85%
attaques Chute Glace, Psykoud'Boul, Eboulement et Croc de Mort passent à 85/85%
attaque Sprint Bouclier passe à 80/85%
attaque Voltageole, Feu Sacré, Explonuit, Ailes Psycho et Jet de Vapeur passe à 85%
attaque Croco Larme passe à 85%
attaques Vampigraine et Toxik passe à 85%
attaque Herblast passe à 150/80%
attaques Typhon Passioné, Typhon Hivernal, Typhon Fulgurant et Typhon Pyrosable passent à 120/80%
attaques Sacrifice et Fulmigraine passe à 120/80%
attaques Souplesse, Pince-Masse et Aéroblast passe à 100/80%
attaque Ecrous d'Poing passe à 60/80%
attaque Ultimawashi passe à 130/75%
attaques Dracocharge et Queue de Fer passent à 110/75%
attaque Trou Noir passe à 70%
attaques Ourange, Massd'Os, Psykoud'Boul et Croc de Mort passent à 30% de flinch
attaques Tête de Fer, Extrasenseur, Lame d'Air et Electrikipik passent à 20% de flinch
attaque Feu Glacé passe à 20% de brûlure et 20% de gelure
attaque Eclair Gelé passe à 20% de paralysie et 20% de gelure
attaque Typhon Hivernal passe à 20% de gelure
attaque Aile d'Acier passe à 30% d'augmentation
attaque Poing Boost passe à 70% d'augmentation
attaque Fulmigraine passe à 10% de baisse
attaque Explonuit passe à 50% de baisse
attaques Souplesse, Sacrifice, Plaie-Croix et Dracogriffe gagnent en taux de critiques
attaques Soin, E-Coque, Repos, Lait à Boire, Paresse, Atterrissage, Appel Soins, Vol-Force, Abri, Pico-Défense, Blockhaus, Blocage, Riposte, Voile Miroir, Tempête Sable, Grêle, Chute de Neige, Spore, Change-Coté, Hurlement, Cyclone, Protection, Mur Lumière, Voile Aurore, Picots, Pics Toxik, Piège de Roc, Toile Gluante, Par Ici, Poudre Fureur, Relais, Clonage et Queulonage passent à 5 PP
attaques Lilliput, Reflet, Malédiction, Appel Défense, Change-Vitesse, Cotogarde, Géo-Controle, Danse Victoire, Mur Fumigène, Décharnement, Grand Nettoyage, Mur de Fer, Amnésie, Allègement, Exuviation, Danse-Lames, Bouclier, Acidarmure, Lumiqueue, Force Cosmik, Gonflette, Plénitude, Danse Draco, Poliroche, Machination, Papillodanse, Enroulement, Hâte, Dernier Mot et Octoprise passent à 5 PP
attaques Berceuse, Poudre Dodo, Siffl'Herbe, Hypnose, Cauchemar, Vantardise, Attraction, Provoc, Tourmente, Feu Follet, Cage Glaciale, Cage-Eclair, Para-Spore, Regard Médusant, Poudre Toxik, Gaz Toxik, Reflet Magik, Lance-Boue, Tourniquet, Vent Arrière, Embargo, Anti-Soin, Prévention, Interversion, Projecteur, Entrave, Téléport, Coup d'Main, Racines, Anneau Hydro, Régénération, Camouflage, Danse-Folle, Détrempage, Poudre Magique, Halloween, Maléfice Sylvain, Renversement, Fil Toxique, Purification, Rune Protect, Brume, Air Veinard, Puissance, Affilage, Conversion et Gaz Corrosif passent à 10 PP
attaques Télékinésie, Danse-Plume, Charme, Chatouille, Ondes Etranges, Grincement, Strido-Son, Croco Larme, Spore Coton, Sécrétion, Aiguisage, Yoga, Nappage, Doux Parfum, Piège de Venin, Croissance et Regard Touchant passent à 10 PP
attaques Brouillard, Flash, Chargeur, Séduction, Larme à l'Oeil, Râle Mâle, Armure, Repli, Boul'Armure, Affutage, Grondement, Rengorgement et Buée Noire passent à 15 PP
attaques Déluge Plasma, Camaraderie, Confidence, Mimi-Queue, Groz'Yeux, Acupression, Conversion 2, Rugissement, Clairvoyance, Oeil Miracle et Flair passent à 20 PP
attaques Mania, Danse-Fleur, Colère, Grand Courroux, Fatal-Foudre, Vent Violent, Pied Voltige, Talon-Marteau, Pince-Masse, Souplesse, Sacrifice, Canicule, Dracocharge, Queue de Fer, Mégacorne, Mégafouet, Eructation, Bang Sonique, Bluff, Escarmouche, Clepto-Manes, Coup Varia-Type, Roue Libre, Regard Glaçant, Fureur Ardente, Coup Fulgurant, Hommage Posthume et Prolifération passent à 5 PP
attaques Pouvoir Antique, Vent Argenté, Vent Mauvais, Lumi-Eclat, Ball'Brume, Rebond, Gyroballe, Punition, Brûloqueue, Vol, Plaquage, Damoclès, Force, Cascade, Croc de Mort, Mâchouille, Stratopercut, Dracogriffe, Electacle, Lame-Feuille, Boutefeu, Canon Graine, Plaie-Croix, Rapaçe, Psykoud'Boul, Tête de Fer, Martobois, Appel Attaque, Tricherie, Double Baffe, Lame Sainte, Peignée, Lancécrou, Tempête Florale, Exécu-Son, Furie-Bond, Fouet de Feu, Botte Sucrette, Bec-Canon, Draco-Marteau, Plasma Punch, Choc Emotionnel, Griffes Funestes, Hache de Pierre, Vagues à Lames, Poing Sonique, Cryo-Pirouette, Ultimapoing, Dard-Nuée, Balle Graine, Stalagtite, Bec Vrille, Balayage, Frappe Atlas, Ombre Nocturne, Retour, Frustration, Poursuite, Facade, Sabotage, Demi-Tour, Change-Eclair, Chloro-Place, Retour de Flamme, Eau Revoir, Direct Toxik, Escalade, Nitrocharge, Désherbaffe, Frotte-Frimousse, Poing Boost, Ancrage, Gliss'Herbe, Lance-Flammes, Surf, Tonnerre, Ball'Ombre, Rayon Signal, Vibrobscur, Lame d'Air, Coup d'Jus, Ebullilave, Ebullition, Pouvoir Lunaire, Boule Pollen, Danse Eveil, Tir de Précision, Voltageôle, Extrasenseur, Aurasphère, Rayon Gemme, Griffe Ombre, Noeud Herbe, Babil, Dévorêve, Lyophilisation, Monte-Tension, Balance, Roulade et Ball'Glace passent à 10 PP
attaques Uppercut, Forte-Paume, Corps Perdu, Estocorne, Ecrasement, Bélier, Massd'Os, Pilonnage, Tranche, Feinte, Etincelle, Force Cachée, Force-Nature, Cogne, Poing Ombre, Aéropiqué, Picore, Coupe Psycho, Poison-Croix, Bombaimant, Piqûre, Balayette, Attrition, Piétisol, Bulldoboule, Centrifugifle, Tranch'Aqua, Rafale Psy, Bulles d'O, Onde Boréale, Détritus, Météores, Dracosouffle, Feuille Magik, Onde de Choc, Vibraqua, Bombe Acide, Parabocharge, Sheauriken, Douche Froide, Poing-Karaté, Koud'Korne, Morsure, Tranch'Herbe, Larcin, Roue de Feu, Aile d'Acier, Queue-Poison, Implore, Crève-Coeur, Dard Mortel, Force Poigne, Double Pied, Cru'Aile, Griffe Acier, Double-Dard, Tranch'Air, Moi d'Abord et Tour Rapide passent à 15 PP
attaques Patience, Torgnoles, Cadeau, Etonnement, Fouet Lianes, Coupe, Vive-Attaque, Léchouille, Mach Punch, Ampleur, Pisto-Poing, Eclats Glace, Ombre Portée, Ecras'Face, Griffe, Charge, Dard-Venin, Picpic, Feuillage, Tapotige, Coud'Boue, Flammèche, Pistolet à O, Vol-Vie, Choc Mental, Poudreuse, Acide, Eclair, Onde Vide, Vent Féérique, Voix Enjôleuse, Vol-Amitié et Tornade passent à 20 PP
attaques Constriction, Faux-Charge et Retenue passent à 30 PP